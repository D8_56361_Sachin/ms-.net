﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.POCO;
using System.Collections;

namespace HR.Utility
{
    public class EmployeeUtils

    {
        
        public  List<Employee> SampleEmpoyee()
        {
            List<Employee> employee = new List<Employee>();
            employee.Add(new Employee() { EmpNo = 1, Name = "Sachin", Designation = "SDE1", Salary = 50000, Commission = 10, DeptNo = 10 });
            employee.Add(new Employee() { EmpNo = 2, Name = "Ramesh", Designation = "SDE2", Salary = 70000, Commission = 10, DeptNo = 10 });
            employee.Add(new Employee() { EmpNo = 3, Name = "Santosh", Designation = "SDE1", Salary = 40000, Commission = 5, DeptNo = 20 });
            employee.Add(new Employee() { EmpNo = 4, Name = "Mahesh", Designation = "SDE2", Salary = 80000, Commission = 10, DeptNo = 20 });
            employee.Add(new Employee() { EmpNo = 5, Name = "Nilesh", Designation = "SDE1", Salary = 50000, Commission = 10, DeptNo = 30 });
            employee.Add(new Employee() { EmpNo = 6, Name = "Nitin", Designation = "SDE2", Salary = 80000, Commission = 10, DeptNo = 30 });
            return employee;
        }

        public void DisplayAllEmployee(List<Employee> employee)
        {
            employee.ForEach(e => { Console.WriteLine("EmpNo = {0}  Name={1} Designation{2} Salary={3} commission= {4}% " +
                                                       "DepartmentNumber={5}",
                                                        e.EmpNo,e.Name,e.Designation,e.Salary,e.Commission,e.DeptNo);});
        }
        
        public void AddEmployee(List<Employee> employeeList, Employee employee)
        {
           
            employeeList.Add(employee); 
        }
        public void CalculateTotalSalary(List<Employee> employeelist)
        {
            double totalSalary = 0;
            int totalNoOfEmployee = employeelist.Count;
            for (int i = 0; i < totalNoOfEmployee; i++)
            {
                totalSalary += employeelist[i].Salary
                    + employeelist[i].Commission * employeelist[i].Salary / 100;
            }
            Console.WriteLine(totalSalary);
        }   
        public void GetAllEmployeeByDept(List<Employee> employeelist, int deptNo)
        {
            int totalNoOfEmployee = employeelist.Count;
            for (int i = 0; i < totalNoOfEmployee; i++)
            {
                 if(employeelist[i].DeptNo == deptNo)
                {
                    Employee e = employeelist[i];
                    Console.WriteLine("EmpNo = {0}  Name={1} Designation{2} Salary={3} commission= {4}% " +
                                                       "DepartmentNumber={5}",
                                                        e.EmpNo, e.Name, e.Designation, e.Salary, e.Commission, e.DeptNo);
                }
            }
        }
        public void GetCountOfEmployeeDepartmentWise(List<Employee> employeelist, List<Department> departmentList)
        
        {
            
            for(int i = 0;i < departmentList.Count; i++)
            {
                int deptWiseEmployee = 0;
                for(int j = 0; j < employeelist.Count; j++)
                {
                    if(employeelist[j].DeptNo == departmentList[i].DeptNo)
                    {
                        deptWiseEmployee += 1;
                    }
                }
                Console.WriteLine("Department No={0}  Departmentwise Employe={1}", departmentList[i].DeptNo, deptWiseEmployee);
            }
        }
         public void GetDepartmentWiseAverageSalary(List<Employee> employeelist, List<Department> departmentList)
        
        {
            
            for(int i = 0;i < departmentList.Count; i++)
            {
                int deptWiseEmployee = 0;
                double totalSalaryDepartmentWise = 0;
                for(int j = 0; j < employeelist.Count; j++)
                {
                    if(employeelist[j].DeptNo == departmentList[i].DeptNo)
                    {
                        deptWiseEmployee += 1;
                        totalSalaryDepartmentWise = totalSalaryDepartmentWise+ (employeelist[j].Salary
                                                    + employeelist[j].Commission * employeelist[j].Salary / 100);
                    }
                }
                Console.WriteLine("Department No={0}  Departmentwise Average Salary ={1}", 
                    departmentList[i].DeptNo, totalSalaryDepartmentWise/ deptWiseEmployee);
            }
        }
        public void GetDepartmentWiseMinimumSalary(List<Employee> employeelist, List<Department> departmentList)

        {

            for (int i = 0; i < departmentList.Count; i++)
            {
               
               
                double[] arrayOfSalary = new double[getNoOfEmployeeInDepartment(employeelist, departmentList[i].DeptNo)];
                List<double> List = new List<double>();
                for (int k = 0;k < arrayOfSalary.Length; k++)
                {
                    for (int j = 0; j < employeelist.Count; j++)
                    {
                        if (employeelist[j].DeptNo == departmentList[i].DeptNo)
                        {
                            // arrayOfSalary[k] = (employeelist[j].Salary
                            //                 + employeelist[j].Commission * employeelist[j].Salary / 100);
                            List.Add(employeelist[j].Salary
                                                 + employeelist[j].Commission * employeelist[j].Salary / 100);
                        }
                    }
                }
                //List.Sort();
                Console.WriteLine("Department No={0}  Departmentwise Minimum Salary ={1}",
                    departmentList[i].DeptNo,List.Min());
            }
        }

        public static int getNoOfEmployeeInDepartment(List<Employee> employeelist, int deptNo)
        {
            int NoOfEmployeeInDepartment = 0;
            for(int i= 0; i < employeelist.Count; i++)
            {
                if(employeelist[i].DeptNo == deptNo)
                {
                    NoOfEmployeeInDepartment += 1;
                }
            }
            return NoOfEmployeeInDepartment;    
        }



    }
}
