﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using HR.POCO;
namespace HR.Utility
{
    public class DepartmentUtils
    {
        public List<Department> SampleDepartment()
        {
            List<Department> list = new List<Department>();
            list.Add(new Department() { DeptNo=10, DeptName= "r&d",Location="Pune"});
            list.Add(new Department() { DeptNo=20, DeptName= "It",Location ="Mumbai"});
            list.Add(new Department() { DeptNo=30, DeptName= "sales",Location ="Chennai"});
            return list;    
        }

        public void DisplayAllDepartments(List<Department> listOfDepartments)
        {
            listOfDepartments.ForEach(d => { Console.WriteLine("DeptNo={0}, DeptName={1}, Location={2}",d.DeptNo,d.DeptName,d.Location); });
        }
        public void AddDepartment(List<Department> departmentList,  Department department)
        {
            departmentList.Add(department);
        }
    }
}
