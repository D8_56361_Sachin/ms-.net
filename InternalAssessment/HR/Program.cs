﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.POCO;
using HR.Utility;
using System.Collections;

namespace HR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            EmployeeUtils employeeUtils = new EmployeeUtils();
            DepartmentUtils departmentUtils = new DepartmentUtils();
            bool flag = true;
            List<Employee> employeelist = employeeUtils.SampleEmpoyee();
            List<Department>departmentList = departmentUtils.SampleDepartment();
            while (flag)
            {
                Console.WriteLine("1:Add Employee  2:Add Department 3:display All Employees " +
                    " 4:Display All Departments  5:Calculate Salary of all Employee  6: Get all employee of particular department " +
                    "7:dept wise count of employee 8:department wise average salary 9:department wise minimum salary  10:Exit");
                Employee employee = new Employee(); 
                Department department = new Department();

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.WriteLine("Enter employee number");
                        employee.EmpNo = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter name of employee");
                        employee.Name = Console.ReadLine();
                        Console.WriteLine("Enter Designation");
                        employee.Designation = Console.ReadLine();
                        Console.WriteLine("Enter salary");
                        employee.Salary = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Enter Commmision in percentage");
                        employee.Commission = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Enter Department number");
                        employee.DeptNo = Convert.ToInt32(Console.ReadLine());
                        employeeUtils.AddEmployee(employeelist, employee);
                        break;
                    case 2:
                        Console.WriteLine("Add Department number");
                        department.DeptNo = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("eneter Department name");
                        department.DeptName = Console.ReadLine();
                        Console.WriteLine("enter Department location");
                        department.Location = Console.ReadLine();
                        departmentUtils.AddDepartment(departmentList, department);
                        break;
                    case 3:
                        employeeUtils.DisplayAllEmployee(employeelist);
                        break;
                    case 4:
                        departmentUtils.DisplayAllDepartments(departmentList);
                        break;
                    case 5:
                        employeeUtils.CalculateTotalSalary(employeelist);
                        break;
                    case 6:
                        Console.WriteLine("Enter Department number");
                        employeeUtils.GetAllEmployeeByDept(employeelist, Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 7:
                        employeeUtils.GetCountOfEmployeeDepartmentWise(employeelist, departmentList);
                        break;
                    case 8:
                        employeeUtils.GetDepartmentWiseAverageSalary(employeelist, departmentList);
                        break;
                    case 9:
                        employeeUtils.GetDepartmentWiseMinimumSalary(employeelist, departmentList);
                        break;


                    case 10:
                        flag = false;
                        break;
                    default: Console.WriteLine("Invalid choice!!!");
                        break;

                }
            }

            Console.WriteLine("Outside of While loop");
            Console.ReadLine();
        }
    }
}
