﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.POCO
{
    public class Employee
    {
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private double _Salary;
        private double _Commission;
        private int _DeptNo;

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public double Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }


        public double Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

    }
}
